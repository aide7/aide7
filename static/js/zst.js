var goodsID = getQueryVariable("id");
loadData(goodsID);
var chart = new F2.Chart({
    id: 'lineChart',
    pixelRatio: window.devicePixelRatio, // 指定分辨率
    padding: ["auto", 10, 30, 30]
});
var cdata = {};
var ChartType = "line";
var Bsize = 10;
var wait = 1000;
var Unike_name = ""
var Umoney = ""
var layer;
var appversion = "android-241";
var codeID = "";
var newc = true;
var socket;
var aData;
var BuycodeSales;
var SaleRecord = 0;
var RQNum = 0;
var CurrPeriod = 0;
var HistoryPeriod = 0;
var codeQuantity = 0;
var oldID;
var loading = true;
var ToBuy = false;

var GoodsUrl = "https://api.yunxiaofu.net/api/lotgoods/v1/app/getgoodsdetailpagedata?goodsID=" + goodsID + "&platform=0";
var ApiDomain = "https://fapi.i40.net.cn"
ApiDomain = "https://q.ptcywl.com"
var gpcp = ["26802", "26803", "26804", "26805", "27352"];
var iSGP = jQuery.inArray(goodsID, gpcp)
if (iSGP != -1) {
    wait = 600
}
//userInfo()
//codeState =4  下架  1上架
//goodsState=1下架   2正常
ZstGetData(1);

layui.use(['layer'], function() {
    layer = layui.layer
});

function Islogin() {
    var token = getCookie("token");
    if (token == null || token == "") {
        localStorage.setItem("CURR_USERINFO", "")
        return false;
    }
    return true
}

function get_baseinfo() {
    var token = getCookie("token");
    if (token == null || token == "") {
        logout();
        return;
    }
    var url = "https://api.yunxiaofu.net/api/member/v1/app/baseinfo";
    $.ajax({
        type: "GET",
        headers: {
            "token": token,
            'channel': 'yxf_android_mobile',
            'appversion': appversion
        },
        contentType: "application/json",
        dataType: "json",
        url: url,
        success: function name(params) {
            console.log(params)
            localStorage.setItem("CURR_USERINFO", "")
        }
    })
}
setInterval(getYE, 12000)

function getYE() {
    var token = getCookie("token");
    if (token == null || token == "") {
        logout();
        return;
    }
    var url = "https://q.ptcywl.com/q.php?action=rafflenum";
    url = "https://api.yunxiaofu.net/api/lotusers/v1/app/rafflenum";
    $.ajax({
        type: "GET",
        headers: {
            "token": token,
            'channel': 'yxf_android_mobile',
            'appversion': appversion,
            "timestamp": Math.round(new Date().getTime() / 1000)
        },
        contentType: "application/json",
        dataType: "json",
        url: url,
        success: function name(params) {
            if (params.code == 401) {
                localStorage.setItem("CURR_USERINFO", "")
                setCookie("token", "")
                delCookie("token")
                return;
            }
            Umoney = params.data
            $("#Ue").html(Umoney);
            setCookie("Umoney", Umoney);
        }
    })
}

var checkTime;

function loadData(goodsId) {
    var goodsPic = localStorage.getItem("goodsPic_" + goodsId);
    var goodsName = localStorage.getItem("goodsName_" + goodsId);
    if (goodsName != null) {
        $(".van-multi-ellipsis--l2").html(goodsName)
        $(".goods-pic").attr("src", goodsPic)
    }
    var getU = localStorage.getItem("CURR_USERINFO");
    if (getU != null && getU != "") {
        getU = JSON.parse(getU);
        $("#Uname").html(getU.userName)
    }
    layer.load(2);
}

function ZstGetData(s = 0) {
    RQNum = RQNum + 1
    var size = $(".searchNum").attr("num");
    var html = "";
    checkTime = setTimeout(checkData, 10);
    $.ajax({
        type: "get",
        url: GoodsUrl,
        success: function name(params) {
            if (typeof(params) == "string") {
                params = JSON.parse(params);
            }
            var row2 = params.data;
            aData = params
            codeQuantity = row2.codeQuantity
            BuycodeSales = row2.codeSales
            CurrPeriod = row2.codePeriod

            localStorage.setItem("goodsPic_" + row2.goodsId, "https://img.yunxiaofu.net/GoodsPic/Pic-200-200/" + row2.goodsPic);
            localStorage.setItem("goodsName_" + row2.goodsId, row2.goodsName);
            var progress = (BuycodeSales / codeQuantity * 100).toFixed(1);
            if (isNaN(progress)) {
                progress = 0
            }
            if (($("#codePeriod").html() != row2.codePeriod) || row2.codeState == 4) {
                $(".van-multi-ellipsis--l2").html(row2.goodsName)
                $("#codePeriod").html(row2.codePeriod)
                $(".need_people").html(codeQuantity)
                $(".goods-pic").attr("src", "https://img.yunxiaofu.net/GoodsPic/Pic-200-200/" + row2.goodsPic)
            }
            $("#buyBtn").html("第" + row2.codePeriod + "期进行中，立即参与 >>")
            $(".now_people").html(BuycodeSales)
            $(".peoples").html(codeQuantity - BuycodeSales)
            $(".progress").html(progress + "%");
            $(".progressBox").css({
                width: progress + "%"
            });
            $(".progress-box-line").css({
                width: progress + "%"
            });
            goods_fight_id = row2["id"]
            id_last = row2["id"];
            codeID = row2["codeId"];
            var ps = Number(progress);
            if (progress < 10) {
                $(".progressA").removeClass("progressA").addClass("progressB")
            } else {
                $(".progressB").removeClass("progressB").addClass("progressA")
            }
            if (row2.codeState == 4) {
                layer.msg("产品已下架")
                return;
            }
            if (row2.codePeriod == 1) {
                layer.closeAll()
                reget(s, wait);
                return;
            }
            if (RQNum == 1) {
                intChart()
                oldID = row2.codePeriod
                ZstH(row2.codePeriod, size, codeQuantity, wait, s)
                return;
            }

            if (RQNum > 1 && (CurrPeriod - 1 == HistoryPeriod == false || s == 0)) { //row2.codePeriod!=oldID
                changeZst();
                ZstH(row2.codePeriod, size, codeQuantity, wait, s)
            } else {
                if (SaleRecord != BuycodeSales && cdata.length > 0) {
                    SaleRecord = BuycodeSales
                    changeZst()
                    reget(s, wait);
                    return;
                }
                reget(s, wait);
                // if(ReGetData()){
                // ZstH(row2.codePeriod,size,codeQuantity,1000,0)
                // }
                return;
            }
        },
        error: function() {
            reget(s, wait)
        },
        fail: function() {
            reget(s, wait)
        }

    })
}


function ZstH(codePeriod, size, codeQuantity, wait, s) {
    clearTimeout(checkTime);
    var zstUrl = ApiDomain + "/api/v1/getHistoryGetter?id=" + goodsID + "&maxPeriod=" + codePeriod + "&count=" + size + "&codeQuantity=" + codeQuantity;
    $.ajax({
        type: "get",
        url: zstUrl,
        success: function name(rd) {
            if (rd == "") {
                ZstH(codePeriod, size, codeQuantity, wait, s)
                return;
            }
            if (rd.code != undefined && rd.code < 0) {
                layer.msg(rd.msg);
            }
            cdata = rd;
            HistoryPeriod = rd[0].codePeriod
            historyH(cdata)
            if (rd[0].coderUserBuyCount == "") { // && cdata.length!=undefined
                $(".historyLists li:eq(0)").addClass("loadColor")
                console.log("reget2")
                    // rd.shift()
                    // console.log("rd="+rd.length)
                    // var loadColorSize=$(".loadColor").size();
                    // if(loadColorSize==0){
                    // oldID=oldID-1;
                    // }
            } else {
                // oldID=codePeriod;
                $(".historylists").attr("codePeriod", codePeriod);
            }
            if (rd[0].reget != undefined) {
                $(".historyLists li:eq(0)").addClass("loadColor")
            }
            reget(s, wait)
            changeZst()
        },
        error: function() {
            reget(s, wait)
        },
        fail: function() {
            reget(s, wait)
        }
    })

}


function reget(s, wait) {
    if (s == 1) {
        setTimeout("ZstGetData(1)", wait);
    }
}


function checkData() {
    var s = $(".loadColor").length;
    if (s > 0) {
        var size = $(".searchNum").attr("num");
        ZstH(CurrPeriod, size, codeQuantity, 1000, 0)
    }

}


function ReGetData() {
    var s = $(".loadColor").length;
    if (s > 0) {
        return true;
    }
    return false;
}

function changeZst() {
    var perioidLen = 9;
    if (10 > cdata.length) {
        perioidLen = cdata.length - 1
    }
    if (cdata.length < 1) {
        console.log(cdata)
        return;
    }
    var lastID = cdata[cdata.length - 1].codePeriod;
    chart.guide().clear();
    chart.scale("codePeriod", {
        ticks: getTicks(lastID, cdata[0].codePeriod, perioidLen),
    });
    for (var n = 0; n < cdata.length; n++) {
        cdata[n].sales = BuycodeSales;
        if (cdata[n].codeIndex == 1 && cdata[n].coderUserBuyCount == "") {
            console.log(cdata[n].codeIndex)
            cdata[n].codeIndex = null;
            oldID = oldID - 1
        }
        if (cdata[n].codeIndex == null || cdata[n].codeIndex == undefined || cdata[n].codeIndex == -1) {
            delete cdata[n].codeIndex
            continue
        }
        chart.guide().tag({
            position: [cdata[n].codePeriod, cdata[n].codeIndex],
            content: "".concat(cdata[n].codeIndex),
            direct: "tl",
            offsetY: -2,
            limitInPlot: true,
            background: {
                fill: "#ffc042"
            },
            pointStyle: { // 点的样式
                fill: '#ffc042', // 填充颜色
                r: 3, // 半径
                lineWidth: 1, // 线的边框
                stroke: '#fff' // 线的描边
            },
            textStyle: { // tag 文本样式
                fontSize: 12,
                fill: '#000'
            },
        });
    }
    chart.changeData(cdata);
    return BuycodeSales;
}



function historyH(row1) {
    if (loading) {
        layer.closeAll()
    }
    var historyHTML = "";
    for (var item in row1) {
        var codePeriod = row1[item].codePeriod;
        var win_num = row1[item].coderNO;
        var nick_name = row1[item].userName;
        var fight_time = row1[item].fight_time;
        var count = row1[item].coderUserBuyCount;
        var Text = "-"
        if (row1[item].codeIndex != null && row1[item].codeIndex != "" && row1[item].codeIndex != undefined && row1[item].codeIndex != -1) {
            Text = getIndexText(row1[item].codeIndex, row1[item].codeQuantity);
        }
        var className = "";
        if (row1[item].coderUserBuyCount == null || row1[item].coderUserBuyCount == "" || row1[item].coderUserBuyCount == "正在揭晓") {
            nick_name = "正在揭晓";
            count = "-";
            Text = "-";
            className = "loadColor"
        }
        if (row1[item].codeIndex == -1) {
            className = "loadColor"
        }

        historyHTML += '<li codePeriod=' + codePeriod + '">\
			<span>' + codePeriod + '</span>\
			<span class="van-ellipsis ' + className + '">' + nick_name + '</span>\
			<span class=' + className + '>' + win_num + '</span>\
			<span class=' + className + '>' + count + '</span>\
			<span class=' + className + '>' + Text + '</span>\
			</li>';
    }
    loading = false;
    $(".historyLists").html(historyHTML)
}

function getIndexText(curIndex, totalCount) {
    const percentage = parseInt(curIndex / totalCount * 100);
    if (percentage < 20) {
        return '头';
    }
    if (percentage < 40) {
        return '前';
    }
    if (percentage < 60) {
        return '中';
    }
    if (percentage < 80) {
        return '后';
    }
    return '尾';
}

function intChart() {
    // chart.clear()
    var bb = [];
    var q = CurrPeriod
    q = q - 1
    for (var i = 0; i < 10; i++) {
        var p = q--
            if (p == 0) {
                break;
            }
        var aa = {}
        bb[i] = new Array()
        aa["codePeriod"] = p
        aa["codeQuantity"] = codeQuantity
        aa["codeSales"] = BuycodeSales
        aa["sales"] = BuycodeSales
        bb[i] = aa
    }
    // console.log(bb)
    newchart(bb)
}


function BarChart(data) {
    var e = [{
        pos: "头",
        count: 0,
        percent: 0
    }, {
        pos: "前",
        count: 0,
        percent: 0
    }, {
        pos: "中",
        count: 0,
        percent: 0
    }, {
        pos: "后",
        count: 0,
        percent: 0
    }, {
        pos: "尾",
        count: 0,
        percent: 0
    }];
    var historyGetter = [{
        pos: "中",
        count: 1,
        percent: 10
    }, {
        pos: "头",
        count: 2,
        percent: 20
    }, {
        pos: "后",
        count: 3,
        percent: 30
    }, {
        pos: "尾",
        count: 3,
        percent: 30
    }, {
        pos: "前",
        count: 1,
        percent: 10
    }];
    // for (e = 0,i = data,n = 0; n < data.length; n++)
    //      i[n].codeIndex ? i[n].codeIndexText = this.getIndexText(i[n].codeIndex, i[n].codeQuantity) : this.openFirst(i[n], this.historyGetTimes);
    // var historyGetter = i,
    var tou = new Array();
    var qian = new Array();
    var zhong = new Array();
    var hou = new Array();
    var wei = new Array();
    //
    for (e = 0, i = data, n = 0; n < data.length; n++) {
        var text = getIndexText(data[n].codeIndex, data[n].codeQuantity);
        if (text == "头") {
            tou.push(text)
        } else if (text == "前") {
            qian.push(text)
        } else if (text == "中") {
            zhong.push(text)
        } else if (text == "后") {
            hou.push(text)
        } else if (text == "尾") {
            wei.push(text)
        }
    }
    historyGetter = [{
        pos: "头",
        count: tou.length,
        percent: tou.length * 10
    }, {
        pos: "前",
        count: qian.length,
        percent: qian.length * 10
    }, {
        pos: "中",
        count: zhong.length,
        percent: zhong.length * 10
    }, {
        pos: "后",
        count: hou.length,
        percent: hou.length * 10
    }, {
        pos: "尾",
        count: wei.length,
        percent: wei.length * 10
    }, ];
    var historyGetterBBB;
    historyGetter.forEach((function(i) {
            for (var n = 0; n < e.length; n++) {
                // console.log("**",i);
                e[n].pos === (e[n].count++,
                    e[n].percent = parseInt(e[n].count / historyGetter.length * 100)
                )
            }
        }))
        // Step 1: 创建 Chart 对象
    const barChart = new F2.Chart({
        id: 'barChart',
        pixelRatio: window.devicePixelRatio, // 指定分辨率
        padding: ["auto", 10, 30, 40]
    });
    barChart.tooltip(!1),
        barChart.animate({
            area: {
                appear: {
                    animation: "groupScaleInY"
                }
            }
        });
    const defs = {
        count: {
            ticks: creatArray(data.length),
        },
    };
    barChart.source(historyGetter, defs);
    barChart.interval().position("pos*count").color("l(90) 0:#2e81ff 1:#eaf2fe"),
        barChart.guide().clear();
    for (var n = 0; n < historyGetter.length; n++) {
        barChart.guide().tag({
            position: [historyGetter[n].pos, historyGetter[n].count],
            content: "".concat(historyGetter[n].percent) + "%",
            direct: "tl",
            offsetY: -2,
            background: {
                fill: "#8659AF"
            }
        });
    }
    barChart.render()
}

function creatArray(l) {
    var n = new Array();
    for (var i = 0; i <= l; i++) {
        n.push(i)
    }
    return n;
}
// F2 对数据源格式的要求，仅仅是 JSON 数组，数组的每个元素是一个标准 JSON 对象。
function newchart(data) {
    for (var n = 0; n < data.length; n++) {
        data[n].sales = aData.data.codeSales;
        data[n].codeSales = aData.data.codeSales;
        // delete data[n].codeIndex
    }
    // BarChart(data)
    // chart.tooltip(!1),
    chart.animate({
        area: {
            appear: {
                animation: "groupScaleInY"
            }
        }
    });
    const defs = {
        codeIndex: {
            ticks: getTicks(0, data[0].codeQuantity, 6),
        },
        codePeriod: {
            ticks: getTicks(data[data.length - 1].codePeriod, data[0].codePeriod, 5),
            range: [0, 1],
            label: {
                textAlign: 'start',
                textBaseline: 'middle',
                rotate: Math.PI / 2
            },
        },
        codeSales: {
            ticks: [0, data[0].codeQuantity]
        },
    };
    var len = 27;
    var rotate = 5;
    var perioidLen = 9;
    if (10 > data.length) {
        perioidLen = data.length - 1
    }
    chart.source(data, {
        codeIndex: {
            ticks: getTicks(0, data[0].codeQuantity, 6)
        },
        codePeriod: {
            ticks: getTicks(data[data.length - 1].codePeriod, data[0].codePeriod, perioidLen),
            range: [0, 1],
        },
        sales: {
            ticks: [0, data[0].codeQuantity]
        }

    });



    if (data[0].codePeriod < 10) {
        len = 10;
        rotate = -12
    } else if (data[0].codePeriod < 100) {
        len = 15;
        rotate = 12
    }
    chart.tooltip({
        showCrosshairs: true,
        showItemMarker: false,
        onShow: function onShow(ev) {
            const items = ev.items;
            // console.log(items)
            items[0].name = null;

            // items[2].name = null;

            items[0].value = "第" + items[0].title + "期";

            if (items.length > 1) {
                items[1].name = null;
                items[1].value = items[1].origin.codeIndex;
            }
        }
    });
    chart.axis("codePeriod", {
            // label: function(t, e, i) {var n = {}; 0 === e ? n.textAlign = "left" : e === i - 1 && (n.textAlign = "right");return n}
            labelOffset: len, // 坐标轴文本距离轴线的距离
            // label: {
            // 		textAlign: 'start',
            // 		textBaseline: 'middle',
            // 		rotate: 5
            // 	}
            label: function(t, e, i) {
                var n = {
                    top: true,
                    textAlign: 'start',
                    textBaseline: 'middle',
                    rotate: rotate
                };
                // var n = {};
                // 0 === e ? n.textAlign = "left" : e === i - 1 && (n.textAlign = "left");
                return n
            }

        }),

        chart.axis("sales", false),
        chart.line().position("codePeriod*codeIndex").color('#fc6307'),
        chart.point().position("codePeriod*codeIndex").color('#fc6307').style({
            stroke: "#fc6307",
            fill: '#fff',
            lineWidth: 1
        }),
        chart.area().position("codePeriod*sales").color("l(90) 0:red 1:#fc6307").shape("smooth"),
        chart.guide().clear();
    for (var n = 0; n < data.length; n++) {
        if (data[n].codeIndex == null || data[n].codeIndex == undefined || data[n].codeIndex == -1) {
            continue
        }
        chart.guide().tag({
            position: [data[n].codePeriod, data[n].codeIndex],
            content: "".concat(data[n].codeIndex),
            direct: "tl",
            offsetY: -2,
            background: {
                fill: "#fc6307"
            }
        });
    }
    newc = false;
    chart.render()

}
// ticks: this.getTicks(0, this.curGoodsInfo.Rows2.codeQuantity, 6)
function getTicksA(t, e, i) {
    console.log(t + "/" + e + "/" + i);
    var n = [t],
        r = e - t,
        a = Math.ceil(r / i);
    a || (a = 1);
    for (var o = 1; o <= i; o++) {
        var s = t + o * a;
        if (s > e) {
            n.push(e);
            break
        }
        n.push(s)
    }
    return n
}

function getTicks(start, end, count) {
    const codeIndexTicks = [start];
    const length = end - start;
    let nickTickInterval = Math.ceil(length / count);
    if (!nickTickInterval) {
        nickTickInterval = 1;
    }
    for (let i = 1; i <= count; i++) {
        const curTickVal = start + i * nickTickInterval;
        if (curTickVal > end) {
            codeIndexTicks.push(end);
            break;
        } else {
            codeIndexTicks.push(curTickVal);
        }
    }
    return codeIndexTicks;
}

$(document).on("click", "#new_fight_time", function() {
    document.querySelector(".goods-info").scrollIntoView()
})

function inputNum() {

    var new_fight_time = Number($('#new_fight_time').val());
    // if (new_fight_time>='113') {
    // $('#jiashang').addClass('cur');
    // }
    // Umoney
    var un = localStorage.getItem("userName")
    var money = localStorage.getItem("money")
    $("#Uname").html(un)
    $("#Umoney").html(money)
    $('#renci-popup').modal('open')
    ToBuy = false;
}
var btn_must = $('#new_fight_time');
var extra = Number($(".peoples").text());
var yy_good_price, yy_good_need, yy_good_now, yy_good_id;

function btn_setnum(i) {
    btn_must.val(i);
    console.log(yy_good_price)
    $('#all_money').html(i * yy_good_price);
    ToBuy = false;
}

function btn_all() {
    var extra = Number($(".peoples").text());
    btn_must.val(extra);
    $('#all_money').html(extra * yy_good_price);
}

function buyNow2() {
    if (ToBuy) {
        layer.msg('请等待下单完成');
        return;
    }
    ToBuy = true;
    var numA = btn_must.val()
    if (Umoney < numA) {
        // layer.msg('余额不足，请先充值');
        // getYE();
        // return;
    }
    //{"codeId":378306,"buyNum":1,"isAutoBuyNext":true}
    var url = "https://api.yunxiaofu.net/api/lotcarttrade/v1/app/submit"
    url = ApiDomain + "/buy/"
    var data = {
        "codeId": codeID,
        "buyNum": Number(numA),
        "isAutoBuyNext": true
    }
    url = "https://api.yunxiaofu.net/api/lotcarttrade/v2/app/submitcarttrade"
    data = {
        "cartGoods": [{
            "buyNum": Number(numA),
            "codeId": codeID
        }],
        "isBuyNext": 1,
        "isCart": 0
    }
    var xdz = layer.msg('下单中...', {
        time: 10000000
    });
    var token = getCookie("token");
    if (token == null || token == "") {
        localStorage.setItem("CURR_USERINFO", "")
        layer.msg('未登录');
        layer.close(xdz)
        return;
    }
    var timestamp = Math.round(new Date().getTime() / 1000)
    ToBuy = true;
    $.ajax({
        type: 'post',
        dataType: "json",
        headers: {
            "token": token,
            "channel": "yxf_android_mobile",
            "appversion": appversion,
            "timestamp": timestamp //Math.floor(Date.now()/1000)
                // ,"signature":signature
        },
        contentType: "application/json",
        url: url,
        data: JSON.stringify(data),
        success: function(data, status, xhr) {
            layer.close(xdz)
                // console.log(status)
                // console.log(xhr.getAllResponseHeaders())
            if (data.code == 3) {
                layer.msg(data.message);
            } else if (typeof(data) == "object" && data.code == 1) { //&& data.boo
                var msg = CurrPeriod + "期" + $("h1").text() + "参与" + btn_must.val() + "次"
                layer.msg(msg, {
                    time: 5000,
                    id: "buyMsg"
                }); //, {icon:1}
                // layer.msg('购买成功'); //, {icon:1}
                setTimeout(getYE, 50)
                setTimeout(function() {
                    ToBuy = false;
                }, 10000)
            } else if (data.code == 401) {
                layer.close(xdz);
                layer.msg(data.message);
                logout();
            } else {
                layer.close(xdz)
                layer.msg(data.message); //, {icon: 2}
                //layer.msg("商品不足", {icon: 2});
            }
            setTimeout("ZstGetData(0)", 20);
            //ZstGetData(false)
        },
        error: function(e) {
            ToBuy = false;
            layer.close(xdz)
            console.log(e.status);
            console.log(e.responseText);
        },
        fail: function(e) {
            ToBuy = false;
        }
    });
}


function buyNow() {
    if (checkNumIsRight()) {
        var xdz = layer.msg('下单中...', {
            time: 100000000000000
        });
        $.ajax({
            type: 'get',
            url: ApiDomain + '/q.php?action=buy&payType=money&codeID=' + codeID + '&ticket_send_id=&goods_fight_ids=' + id_last + '&goods_buy_nums=' + btn_must.val(),
            success: function(data, status, xhr) {
                layer.close(xdz)
                console.log(status)
                console.log(xhr.getAllResponseHeaders())
                    //console.log(data)
                if (data.code == 3) {
                    layer.msg(data.message);
                } else if (typeof(data) == "object" && data.code == 1) { //&& data.boo
                    layer.msg('购买成功'); //, {icon:1}
                    getYE();
                    BuycodeSales = BuycodeSales + Number(btn_must.val())
                    changeZst();
                } else {
                    layer.close(xdz)
                    layer.msg(data.message); //, {icon: 2}
                    // layer.msg("商品不足", {icon: 2});
                }
                ZstGetData(0);
            },
            error: function(e) {
                layer.close(xdz)
                console.log(e.status);
                console.log(e.responseText);
            }
        });
    }
}

function checkNumIsRight() {
    ToBuy = false;
    var new_fight_time = Number($('#new_fight_time').val());
    var last_fight_time = Number($(".peoples").html());
    var good_single_price = Number('1');
    if (new_fight_time > last_fight_time) {
        //$('#new_fight_time').val(good_single_price);
        checkIsNoClick();
        layer.msg("您输入的数量已经超过最大可买数量,请重新输入!");
        $('#new_fight_time').val(codeQuantity - SaleRecord)
        $('#my-alert-sure').attr("onclick", "$('#new_fight_time').focus()");
        return false;
    } else if (new_fight_time == 0 || new_fight_time < 0 || new_fight_time == '') {
        checkIsNoClick();
        $('#my-alert-sure').attr("onclick", "$('#new_fight_time').focus()");
        layer.msg("您输入的数量不合法,请重新输入!");
        return false;
    } else {
        $('#new_fight_time').val(new_fight_time);
        checkIsNoClick();
        return true;
    }
}

function checkIsNoClick() {
    var new_fight_time = Number($('#new_fight_time').val());
    var last_fight_time = Number($(".peoples").html());
    if (new_fight_time <= 1) {
        $('#jianqu').addClass('cur');
    } else {
        $('#jianqu').removeClass('cur');
    }
    if (new_fight_time >= last_fight_time) {
        $('#jiashang').addClass('cur');
    } else {
        $('#jiashang').removeClass('cur');
    }
}

function countFight(type) {
    var new_fight_time = Number($('#new_fight_time').val());
    if (type == '-') {
        if (!$('#jianqu').hasClass('cur')) {
            new_fight_time = new_fight_time - 1;
            if (new_fight_time < 1) {
                return;
            }
            $('#new_fight_time').val(new_fight_time);
            checkIsNoClick()
        }
    } else {
        if (!$('#jiashang').hasClass('cur')) {
            new_fight_time = new_fight_time + 1;
            $('#new_fight_time').val(new_fight_time);
            checkIsNoClick()
        }
    }
}
jQuery(document).on("focus", "#new_fight_time", function(a) {
    $(this).val("");
})
$("#ljxd").click(function() {
    if (Islogin()) {
        setTimeout(getYE, 10)
        inputNum()
    } else {
        // loginBox()
        if (top != self) {
            window.parent.document.getElementById("frame_d").contentWindow.location.href = "/login.html"
        } else {
            location.href = "login.html"
        }


    }
})
$(".jiaBtn").click(function() {
    var o = $(".searchNum");
    var oval = o.val();
    o.val(Number(oval) + 1);
})
$(".jianBtn").click(function() {
    var o = $(".searchNum");
    var oval = o.val();
    if (Number(oval) < 5) {
        return;
    }
    o.val(Number(oval) - 1);
})
$(".buttonSearch").click(function() {
    var num = $(".searchNum").val()
    if ($(".searchNum").val() > 100) {
        num = 100;
    }
    $(".searchNum").attr("num", num)
    ZstGetData(0)
})

function qh(CType) {
    if (CType == "bar") {
        $(".lineBox").hide();
        // $(".van-tab").removeClass("van-tab--active");
        $(".barBox").show();
    } else {
        $(".lineBox").show();
        $(".barBox").hide();
    }
    // newchart(cdata);
}
$(".van-tab").click(function() {
    $(".van-tab").removeClass("van-tab--active");
    $(this).addClass("van-tab--active");
})

function setCookieOld(name, value, stime = 24 * 365) {
    var exp = new Date();
    exp.setTime(exp.getTime() + stime * 60 * 60 * 1000);
    document.cookie = name + '=' + escape(value) + ';path=/;expires=' + exp.toGMTString();
}

function setCookie(name, value, stime) {
    var topHost = location.host.replace(/^\w+\./, "");
    var exp = new Date();
    var stime = stime || 24 * 365
    exp.setTime(exp.getTime() + stime * 60 * 60 * 1000);
    document.cookie = name + '=' + escape(value) + ';path=/;domain=.' + topHost + ';expires=' + exp.toGMTString();
    //document.cookie = name + '='+ escape (value) + ';path=/;expires=' + exp.toGMTString();
}

function getCookie(name) {
    var arr, reg = new RegExp('(^| )' + name + '=([^;]*)(;|$)', 'i');
    if (arr = document.cookie.match(reg)) return unescape(arr[2]);
    else return null;
}

function rt(i) {
    return i.replace(/[^\d]/g, "")
}

function rkg(i) {
    return i.replace(/[\r\n\s\t\f ]/g, "")
}

function checkAndSumbit() {
    var phone = rkg($('#mobile').val().trim());
    var yzm = $('#Ycode').val();
    if (!phone || typeof(phone) == "undefined" || phone == 0 || phone == '') {
        layer.msg("您尚未输入手机号码!");
        return false;
    }
    if (!yzm || typeof(yzm) == "undefined" || yzm == '') {
        layer.msg("验证码不能为空!");
        $('#Ycode').focus();
        return false;
    }
    console.log(phone)
        // yzm=yzm.replace(/[^\d+]/g,"").substring(0,6);
    $('#submitBut').attr("disabled", true)
    $.ajax({
        type: "post",
        url: ApiDomain + "/q.php?action=login",
        dataType: "json",
        data: "mobile=" + phone + "&password=" + yzm,
        success: function(data) {
            var result = data;
            if (typeof(result) != "object") {
                result = JSON.parse(result);
            }
            if (result.code == 1) {
                layer.msg("登录成功");
                setCookie("tel", phone);
                uid = phone;
                localStorage.setItem("CURR_USERINFO", JSON.stringify(result.data))
                $('#submitBut').attr("disabled", false)
                layer.closeAll();
                ZstGetData(0);
                $("#ljxd").click()
            } else {
                layer.msg(result.message);
                // setCookie("uid",result.uid);
                // console.log(result.uid)
                // window.uid=result.uid;
                $('#submitBut').attr("disabled", false)
                    // location.reload();
            }
        },
        error: function() {
            layer.msg("登录错误");
            $('#submitBut').attr("disabled", false)
        },
        fail: function() {
            layer.msg("登录异常错误");
            $('#submitBut').attr("disabled", false)
        }
    });
}

function daojishi(obj) {
    var obj = $(obj);
    obj.attr("disabled", "disabled"); /*按钮倒计时*/
    var time = 60;
    var set = setInterval(function() {
        obj.html(--time + "s");
    }, 1000); /*等待时间*/
    setTimeout(function() {
        obj.attr("disabled", false).html("重新获取验证码"); /*倒计时*/
        clearInterval(set);
    }, 60000);
}

document.addEventListener("WeixinJSBridgeReady", function() {
    bgm();
}, false);

function bgm() {
    var audio = document.getElementById("yx_player");
    audio.setAttribute("src", "https://web-1251406747.file.myqcloud.com/skin/images/win2.mp3");
    audio.play();
}

$("#voiceTip").click(function() {
    var o = $(this).attr("on");
    if (o == "1") {
        $(this).attr("on", "0");
        setCookie("voice", "0")
        $("#voiceTip").html("<img src='https://w.5smz.cn/skin/images/shengyin2.png'>");
    } else {
        setCookie("voice", "1")
        $(this).attr("on", "1");
        $("#voiceTip").html("<img src='https://w.5smz.cn/skin/images/shengyin.png'>");
    }
})

function loginBox() {
    var ah = document.body.clientHeight;
    var h = ah / 2 - 130;
    if (ah < 530) {
        h = h - 50;
    }
    layer.open({
        skin: 'login',
        type: 1,
        id: 'login' //防止重复弹出
            ,
        offset: h + "px",
        area: ['96%', '260px'],
        anim: 6,
        content: $("#loginTpl").html(),
        title: 0,
        success: function(layero, index) {
            var t = getCookie("tel");
            if (t != null) {
                $("#mobile").val(t)
            }
        },
        cancel: function() {}
    })
}


function progressSet(progress) {
    if (progress == 0) {
        $(".progress").removeClass("progressA")
        $(".progress").addClass("progressB")
    }
}


function btn_5() {
    btn_must.val(10);
    $('#all_money').html(10 * yy_good_price);
}

function btn_10() {
    btn_must.val(20);
    $('#all_money').html(20 * yy_good_price);
}

function btn_15() {
    btn_must.val(50);
    $('#all_money').html(50 * yy_good_price);
}


function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
    return (false);
}

// var qrcode = new QRCode(document.getElementById("qrcodeBox"), {
// 	width : 300,
// 	height : 300,
// 	colorDark : "#000000",
//     colorLight : "#ffffff",
// 	correctLevel : QRCode.CorrectLevel.L
// });
// qrcode.makeCode(location.href);


function logout() {
    setCookie("token", "");
    setCookie("userWeb", "");
    localStorage.removeItem("userWeb")
    localStorage.removeItem("CURR_USERINFO")
    socket.io.disconnect()
}


function userInfo() {
    var Url = ApiDomain + "/q.php?action=userInfo";
    $.ajax({
        type: "get",
        url: Url,
        success: function name(rd) {
            console.log(rd)
        },
        error: function() {},
        fail: function() {}
    })
}


setTimeout(function() {
    $("[id^='_']").remove();
    $("[id^='udp_']").remove();
}, 500);




var agent = navigator.userAgent.toLowerCase();
var iLastTouch = null;
if (agent.indexOf('iphone') >= 0 || agent.indexOf('ipad') >= 0) {
    document.body.addEventListener('touchend', function(event) {
        var a = new Date().getTime();
        iLastTouch = iLastTouch || a + 1;
        var c = a - iLastTouch;
        if (c < 500 && c > 0) {
            event.preventDefault();
            return false;
        }
        iLastTouch = a;
    }, false);
};
