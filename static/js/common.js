if (navigator.userAgent.indexOf("Trident") != -1) {
    if (!localStorage.ie || localStorage.ie == "10") {
        localStorage.ie = 1;
        layer.open({
            title: "请更换浏览器",
            content: "你使用的是不支持的IE系列浏览器，<br/>请更换360浏览器、谷歌浏览器、国产浏览器<br/>即可正常使用<br/>国产浏览器需选择兼容模式"
        })
    }
    localStorage.ie++;
}

